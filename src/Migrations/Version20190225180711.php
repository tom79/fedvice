<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190225180711 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE account (id INT AUTO_INCREMENT NOT NULL, uuid VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag (id INT AUTO_INCREMENT NOT NULL, tag VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notifications_logs (id INT AUTO_INCREMENT NOT NULL, last_read_id VARCHAR(255) NOT NULL, date DATETIME NOT NULL, new_notifications INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE instance (id INT AUTO_INCREMENT NOT NULL, host VARCHAR(255) NOT NULL, social VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE instance_tag (instance_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_B61E2BB3A51721D (instance_id), INDEX IDX_B61E2BBBAD26311 (tag_id), PRIMARY KEY(instance_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE instance_account (instance_id INT NOT NULL, account_id INT NOT NULL, INDEX IDX_BAA29B673A51721D (instance_id), INDEX IDX_BAA29B679B6B5FBA (account_id), PRIMARY KEY(instance_id, account_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE instance_tag ADD CONSTRAINT FK_B61E2BB3A51721D FOREIGN KEY (instance_id) REFERENCES instance (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE instance_tag ADD CONSTRAINT FK_B61E2BBBAD26311 FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE instance_account ADD CONSTRAINT FK_BAA29B673A51721D FOREIGN KEY (instance_id) REFERENCES instance (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE instance_account ADD CONSTRAINT FK_BAA29B679B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE instance_account DROP FOREIGN KEY FK_BAA29B679B6B5FBA');
        $this->addSql('ALTER TABLE instance_tag DROP FOREIGN KEY FK_B61E2BBBAD26311');
        $this->addSql('ALTER TABLE instance_tag DROP FOREIGN KEY FK_B61E2BB3A51721D');
        $this->addSql('ALTER TABLE instance_account DROP FOREIGN KEY FK_BAA29B673A51721D');
        $this->addSql('DROP TABLE account');
        $this->addSql('DROP TABLE tag');
        $this->addSql('DROP TABLE notifications_logs');
        $this->addSql('DROP TABLE instance');
        $this->addSql('DROP TABLE instance_tag');
        $this->addSql('DROP TABLE instance_account');
    }
}
