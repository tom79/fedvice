<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NotificationsLogsRepository")
 */
class NotificationsLogs
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $last_read_id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     */
    private $new_notifications;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastReadId(): ?string
    {
        return $this->last_read_id;
    }

    public function setLastReadId(string $last_read_id): self
    {
        $this->last_read_id = $last_read_id;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getNewNotifications(): ?int
    {
        return $this->new_notifications;
    }

    public function setNewNotifications(int $new_notifications): self
    {
        $this->new_notifications = $new_notifications;

        return $this;
    }
}
