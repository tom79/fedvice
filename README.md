### How to install

1. Clone the repository

2. In root directory copy `.env.example` in `.env` 

3. Install vendors:<br/><br/>
`composer install`<br/><br/>
PS: You need to install `composer`, just use:<br/><br/>
`apt install composer`

4. Change the database connection<br/><br/>
Edit `.env` file, and change these lines with your values<br/><br/>
`DATABASE_URL=mysql://db_user:db_pwd@db_host:db_port/db_name`

5. Initialize database:<br/><br/>
`php bin/console doctrine:database:create`<br/>
`php bin/console doctrine:migrations:migrate`

5. Create the bot<br/><br/>
a. From the website, click on preferences and then Development<br/>
![](img/pic01.png)<br/><br/>
b. Click on "NEW APPLICATION"<br/>
![](img/pic02.png)<br/>
c. Fill the form<br/>
![](img/pic03.png) (make sure the app is on read + write which are default values)<br/>
d. Get your access token<br/>
![](img/pic04.png)<br/>

6. Set your token<br/><br/>
Edit `.env` and change these values<br/><br/>
`app_host=HOST_NAME_OF_THE_ACCOUNT`<br/>
`app_token=YOUR_TOKEN_HERE`
<br/><br/>
`app_host` is the domain of the instance hosting the bot account<br/>
`app_token` is the string that you get previously (access token)

7. Crontab<br/><br/>
Change the path to `bin/console` and set your own frequency
```
# m h  dom mon dow   command
* * * * * php /path/to/fedvice/bin/console read:notifications
```